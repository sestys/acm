#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
	int testCases;
	unsigned long premium;
	int farmers;
	int area, animals, dotation;

	scanf("%i", &testCases);
	for (int i = 0; i < testCases; i++) {
		premium = 0;
		scanf("%i", &farmers);
		for (int j = 0; j < farmers; j++) {
			scanf("%i %i %i", &area, &animals, &dotation);
			premium += area * dotation;
		}
		printf("%lu\n", premium);
	}
	return 0;
}